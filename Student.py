class Person:
  def __init__(self):
    self.name = input(f'Enter the name of Student')
    self.age = input(f'Enter the age of student')

  def display(self):
    print(f'Name: {self.name}\nAge: {self.age}')

class Student(Person):
  def __init__(self):
    self.person = Person()
    self.section = input(f'Enter the section of student')
    
  def display_student(self):
    print(f'Name: {self.person.name}\nAge: {self.person.age}\nSection: {self.section}')

#v = Person()
#v.display()
s = Student()
s.display_student()
